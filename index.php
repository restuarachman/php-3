<?php
require("ape.php");
require("frog.php");
    $sheep = new Animal("shaun");
    $sheep->get_info();
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->get_info();
    echo "jump : ";
    $kodok->jump();
    echo "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    $sungokong->get_info();
    echo "yell : ";
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";
?>